import Vue from 'vue'
import Router from 'vue-router'
import Principal from '../components/Principal'
import InicioAsesores from '../components/InicioAsesores'
import RegistroAsesorados from '../components/RegistroAsesorados'
import RegistroAsesores from '../components/RegistroAsesores'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Principal',
      component: Principal
    },
    {
      path: '/InicioAsesores',
      name: 'InicioAsesores',
      component: InicioAsesores
    },
    {
      path: '/RegistroAsesorados',
      name: 'RegistroAsesorados',
      component: RegistroAsesorados
    },
    {
      path: '/RegistroAsesores',
      name: 'RegistroAsesores',
      component: RegistroAsesores
    }
  ]
})
